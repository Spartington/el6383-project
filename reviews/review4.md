
Project Review
=====================================================


## Overview

1) Briefly summarize the experiment in this project.

The experiment aims at studying the effect of 'Policing' and
'Shaping' policies in keeping the demanded throughput. The experiment tests the throughput
under totally fourteen different network environments.

2) Does the project generally follow the guidelines and parameters we have
learned in class?

Actually, the project violates several guidelines we have learned in class. The experiment design should be improved to enhance reliablity
and reflect more information. The way to communicating results is to some extent, misleading.


## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?

The goal of this experiment is to see how closely one can get the actual throughput of a connection to match the desired
limit, while using different policies such as 'Policing' and 'Shaping'. It is a focused goal that leads to a specific experiment.
It aims at discovering the effect of different policies so it is useful and may have interesting results.


2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?

The metric and parameters are chosen appropriately and the experiment design clearly supports the goal.

3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?

In this experiment, the burst rate is fixed to be 6% of the max rate. The experiment compares the throughput under seven different settings.
But the difference between settings is only the max rate and the fixed portion of burst rate so the meaning to have that many settings
is not significant. What could be an improvement is to vary the ratio of burst rate to max rate so that the effect of this ratio can be observed.
In addition, for each experiment unit, only one test is conducted. This is not reasonalbe and will reduce the significance of the results
considering the existence of variance and error.


4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?

The metric is exactly the right metric since the goal for this experiment is to compare between the throughput and the bandwidth
demanded under different policies so the
throughput is the right metric.

5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?

The max rate parameters is meaningful. It varys from 1mbps to 50mbps which is a range in most network environments.
But the burst rate is not representative and meaningful enough since it is set to be 6% of the max rate, which is a seemingly random percentage.

6) Have the authors sufficiently addressed the possibility of interactions
between parameters?

The clarification for the possiblility of interactions between parameters is not sufficient. The author only mentions this aspect in
the proposal for the relating question and has no explanation in the final report. Also the setting of the experiment units is not
sufficient for discovering all the possible interactions between parameters.


7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?

The comparisons are made reasonably. The baseline is appropriate and realistic.

## Communicating results


1) Do the authors report the quantitative results of their experiment?

The author do report the quantitative results.

2) Is there information given about the variation and/or distribution of
experimental results?

There is not information about the variation or distribution of the results since only one trial is performed for each case.

3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?

No, the author interprets the data in a misleading way trying to enlarge the significance of the difference. It is noticeable that the scale of the y-axis is not consistent: one
block in the y-axis represents different number. In fact the difference between the two policies is not that obvious: 1.28 in the case of
50. By varying the y-scale, the author intentionally enlarge the difference between the two policies. Also, the plot does not provide confidence interval for comparison and thus
the significance of difference is doubtful.

4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?

The data is not presented clearly and effectively enough. The goal of this experiment requires a comparison between the throughput
and the demanded rate but the demanded rate is reflected by the x-axis, which is
not an effective way for comparison. The author should choose some better way to serve the goal.

5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?

The conclusion is not sufficiently supported. Data is collected only once for each case so there is no way to estimate the influence
of variance in the comparison, which reduces the reliablity and the signicicance of the result. Also the author interprets the result in
a misleading way which makes the conclusion of the experiment doubtful.

## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?

The author includes instructions for reproducing the experiment in all 3 ways. The instructions are clear and easy to understand.


2) Were you able to successfully produce experiment results?

Actually, there are serveral obvious typos in the instruction for setting the burst rate in policing. But it can be easily corrected and I was
able to successfully produce the results.

3) How long did it take you to run this experiment, from start to finish?

About 6 hours.

4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?

I had to fix the typos in the instructions for setting the burst rate in policing but it was quite easy. I did not need to make any further changes.  

5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?

It should be of degree 3.

## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.

In your code for R script `q <- q + scale_y_discrete("Throughput (Mbps)")`, the y scale should be continuous. Otherwise, the y-scale is not consistent.  
