
Project Review
=====================================================


## Overview

1) Briefly summarize the experiment in this project.

The experiment in this project aims to see which one of policing and shaping are most effective for traffic control. The understanding is that on a larger scale, each user has a fixed bandwidth. The experiment is trying to determine which traffic control method gets the actual user bandwidth closest to this fixed bandwidth.


2) Does the project generally follow the guidelines and parameters we have
learned in class?

Generally, the project does follow most of the guidelines and parameters we have followed in class. However, a few parts of the report could have been more clearer such as why was OVS considered for the policing case and tc for the shaping case. Also, the quantitative result communication of the experiment could have been slightly more detailed (explained in additional comments)


## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?

**Ans.** As per the project report, the goal of the experiment is to see how closely one can get the actual throughput of a connection to match the desired limit, while using different policies such as 'Policing' and 'Shaping'. The goal goes on to state the parameters to be varied (max rate and burst) and metric to be measured (throughput). The goal is specific since it clarifies on the experiment setup as well - a connection with 2 nodes.

Although this is a useful topic, I think that it would have been slightly more representative of real life to consider multiple hosts communicating with a server, rather than just a single host to a server.

2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?

**Ans.** The metrics and parameters chosen for this experiment goal are appropriate for the experiment goal.

The experiment design of a host and server does support the experiment goal clearly. However, it might have been more appropriate to take more than one host communicating with the server in order to more accurately see the impact of shaping and policing.

3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?

**Ans.** Yes, with the given experiment design maximum information can be obtained from a reasonable number of trials. Ofcourse, the more the trials - the more we can account for variance, standard deviation and other indicative measures of the range of values.

4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?

**Ans.** The metric chosen for this experiment was the resultant throughput. I think that given the experiment goal and design, this is a clear metric and should lead to a correct conclusion.

5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?

**Ans.** The parameters being varied here are policy and desired rate. The desired burst was set at 6% of the desired rates throughout the runs. For the policies being studied, the range over which the desired range is varied is appropriate. The experiment designers have addressed the fact that the desired burst rate could be varied as well (in their experiment results), which could be the future scope of the project.

6) Have the authors sufficiently addressed the possibility of interactions
between parameters?

**Ans.** To quote the author's project proposal - they would like to see the general trend of one policy becoming more accurate/inaccurate as the parameters are varied. Since the desired burst is kept at a constant percentage throughout the runs, the authors are only addressing the interaction between the rates and the policy type.

7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?

**Ans.** Yes, the comparisons are reasonable and the baseline selected for comparison is also realistic (it is common for a host to have a 1Mbps or higher bandwidth line allocated to them in real life).


## Communicating results


1) Do the authors report the quantitative results of their experiment?

**Ans.** The authors have only provided a graphical representation of the experimental results. Although this is effective, I think that having the actual numerical values listed along with the graph would be clearer.

2) Is there information given about the variation and/or distribution of
experimental results?

**Ans.** No, there was no information provided about the variation and distribution of results (information such as averages, number of runs, standard deviations of a particular scheduling policy, etc were missing). The only information provided about the results was the attached graph.

3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?

**Ans.** The authors have made a mistake in calculations which negatively impacts the integrity of the results. Noting the fact that the authors said that the burst would be set at 6% of the desired rate *throughout* the experiment, I observed that the correct values for burst were not set in the report commands for Policing. Although this might have been unintentional, it does affect the experiment results conversely.

4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?

**Ans.** Considering the experiment goal, the bar graph is the appropriate graph to represent the obtained data. For the data collected though, I think that fixing values on the y-axis to be the same as the values on the x-axis (1,5,10...) might have made interpreting the results of the graph easier.

5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?

**Ans.** Since the experiment results of the Policing half of the experiment were not completely accurate (as explained in Ans 3), the conclusions drawn cannot be sufficiently supported by the authors results.

## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?

**Ans.** The authors have included instructions for getting results from the raw data and setting up the experiment from scratch. However, I did not find clear instructions on using their existing experiment setup.

2) Were you able to successfully produce experiment results?

**Ans.** Yes, on following the instructions provided by the authors - I was able to obtain similar experiment results (with minor variation in values obtained).  

3) How long did it take you to run this experiment, from start to finish?

**Ans.** It took approximately 2 hours to run this experiment from start to finish.

4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?

**Ans.**  One change had to be made while reserving resources for the experiment. The rspec file used the NYU instageni aggregate but I was unable to sign into the reserved resources using this aggregate. Hence I used the Missouri aggregate to reserve resources and I was able to complete the rest of the experiment with the provided documentation.  Figuring out this step did not consume a significant amount of time.


5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?

**Ans.** The given experiment would fall in degree 3 of reproducibility. This is because I was able to reproduce the experiment results with a certain amount of effort (greater than 15minutes hence not level 5 and did not require proprietory tools hence not level 4).



## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.

As mentioned earlier in the review, the authors should reconsider the results from the Policing half of the experiment with the appropriate values (ie. set burst values correctly to 6% of desired rate). This might change the results of the experiment. It might also be interesting to consider 2 hosts instead of 1 and see what impact policing/shaping has on traffic control in this case (as it might be slightly more realistic).
