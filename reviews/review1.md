
Project Review
=====================================================


## Overview

1) Briefly summarize the experiment in this project.
The goal of this experiment is to see how closely we can get the actual throughput of a connection to match the desired limit, while using different policies such as 'Policing' and 'Shaping'. The max rate and burst of a connection between two nodes will be varified, and then we’ll measure the throughput. 'OVS' is used for the Policing case, while 'tc' will be used for the shaping case.


2) Does the project generally follow the guidelines and parameters we have
learned in class?
Yes. The project used several points that we learned in the past experiments. This experiment used netperf which is very useful here.



## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?
The goal of this experiment is to see how closely we can get the actual throughput of a connection to match the desired limit, while using different policies such as 'Policing' and 'Shaping'. The max rate and burst of a connection between two nodes will be varified, and then we’ll measure the throughput. 'OVS' is used for the Policing case, while 'tc' will be used for the shaping case.
Absolutely it’s a focused goal and is useful and likely to have interesting results.


2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?
Yes. The metrics and parameters chosen are appropriate for this goal. The experiment design can clearly support the experiment. The Netperf tool can measure the throughputs in “policing ” and “shaping” methods. So it’s a good metric to achieve our goal.

3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?
The author used not that enough number of trials to get their conclusions. However, the author only ran one for each condition so that it may cause some abnormal statics. For example in my first trial of “shaping”, the output seems strange here. I think the author should run several trials in one condition to avoid the error.

4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?
I think the metrics that the author used are good enough to lead to the conclusion. The metrics are clear, and suited for the experiment.

5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?
Yes, the netperf output could give the performance of throughput. Netperf is a software application that provides network bandwidth testing between two hosts on a network. The ranges are also parameters.

6) Have the authors sufficiently addressed the possibility of interactions
between parameters?
Yes. The author chose several different parameters that represents different conditions to compare the results in Policing and Shaping.


7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?
The comparisons are made reasonably and the baseline selected for comparison appropriate and realistic.


## Communicating results


1) Do the authors report the quantitative results of their experiment?
Yes, the authors use R tool to report all the results and made a graph which is more intuitionistic.

2) Is there information given about the variation and/or distribution of
experimental results?
Yes. The authors showed that in the graph, it appears that as the bandwidth limit rate increased, the Shaping policy did the best job at staying as close as possible to the set limit. What can also be seen from the graph is that the difference between Shaping and Policing for each set rate limit remains pretty constant throughout the experiment.

3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?
Yes, the data that the author provided to us could clearly support his opinion which could mean that the set burst parameter of 6% could be more beneficial to Shaping and Policing, and perhaps Policing operates better at a different percentage of burst.

4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?
Yes. First, the authors show all the results clearly. More than that, they also provide the graphical form by R. So it helps us easily understand the comparison between Policing and Shaping. The graph is appropriate for the “story” that the data is telling.

5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?
Yes, the conclusions drawn by the authors sufficiently matched the results.
## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?
Yes, the steps the authors provided are very clear. With these instructions, I can easily reproduce the experiment.

2) Were you able to successfully produce experiment results?
Yes, I can successfully produce experiment results with the instructions.

3) How long did it take you to run this experiment, from start to finish?
About two hours.

4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?
I didn’t need to make any changes about this experiment. But from my results, the result from the first condition of Shaping seems strange. I think it may caused by some error that is not caused by the experiment. So if adding the number of trials of each conditions, we may have a more precise result.

5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?
I would give degree 4 to this experiment. The experiment instructions for the R is not detailed enough and I think we can add number of trials for each condition to keep the accuracy.


## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.
This is a very good experiment that makes me comfortable to reproduce. There is barely any problems in this experiment. Now for the nitpicks, the instructions of the R can be a little more detailed. And also, some more trials for each condition could be added to make the result more reasonable.
